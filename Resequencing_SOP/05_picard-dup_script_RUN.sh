#Picard MarkDuplicates

#cd /home/shaferab/DEER/DATA_ALIGNED_files
#mkdir dups; mkdir dups/dups_slurm; mkdir dups/flagstat; mkdir dups/metrics; cd mapped
mkdir dups_slurm
for f in $(ls *.bam | sed 's/.sorted_reads.bam//' | sort -u)
do
echo ${f}
sbatch -o /home/shaferab/DEER/DATA_ALIGNED_files/dups_slurm/${f}-%A.out 05_picard-dup.sh ${f}
sleep 10
done
