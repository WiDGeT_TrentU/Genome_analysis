#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=angsd
#SBATCH --mem 256G
#SBATCH --cpus-per-task 10
#SBATCH --time=04-00:00
module load angsd
angsd -bam mpileup_list -doMajorMinor 1 -domaf 1 -out deer.angsd -doVcf 1 -nThreads 10 -doGeno -4 -doPost 1 -gl 2 -SNP_pval 1e-6 -minMapQ 20 -minQ 20 -doCounts 1 -skipTriallelic 1 -doGlf 2
#note the VCF output is 4.2 and can't be read by vcftools as is; continuing through until phasing will produce a readable VCF

#Editing angsd vcf to be compatable with further steps and vcftools
#make file that includes new header called vcf_head
#in this file will be: ##fileformat=VCFv4.2
#Delete the first row containing ##fileformat=VCFv4.2(angsd version)
sed 1d ANGSD_OUTPUT.vcf > FILE.vcf
#insert new first line with: ##fileformat=VCFv4.2
cat vcf_head FILE.vcf > FILE2.vcf

#essentially the first line of the vcf file changes from:
##fileformat=VCFv4.2(angsd version)
#to:
##fileformat=VCFv4.2