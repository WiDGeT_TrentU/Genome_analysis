#!/bin/bash
#SBATCH --time=0-16:00
#SBATCH --account=rrg-shaferab
#SBATCH --mem=8G
module load bwa
module load picard
module load gatk/3.8
module load samtools
java -Xmx8G -jar $EBROOTPICARD/picard.jar CreateSequenceDictionary R=wtdgenome1.fasta O=wtdgenome1.dict
samtools faidx wtdgenome1.fasta
bwa index wtdgenome1.fasta -a bwtsw


#should merge with sript 07