#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=bcftools_concat
#SBATCH --cpus-per-task=1
#SBATCH --mem=48G
#SBATCH --time=00-03:00

module load bcftools
module load samtools

ls -d freebayes_run/*.vcf > freebayes_file.list
bcftools concat \
--file-list /home/shaferab/DEER/freebayes_file.list \
--output /home/shaferab/DEER/DEER_freebayes_raw.vcf \
--output-type v && \
echo your job is complete
#END

