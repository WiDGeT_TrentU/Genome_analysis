#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=bcftools_concat
#SBATCH --cpus-per-task=1
#SBATCH --mem=48G
#SBATCH --time=00-03:00

module load bcftools
module load samtools

ls -d gatk_run/*.vcf > gatk_file.list
bcftools concat \
--file-list /home/shaferab/DEER/gatk_file.list \
--output /home/shaferab/DEER/DEER_gatk_raw.vcf \
--output-type v && \
echo your job is complete
#END
