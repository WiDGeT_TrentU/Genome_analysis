#!/bin/bash
#MAP_script_RUN

# Genome location
g="/home/shaferab/projects/rrg-shaferab/shaferab/DEER/wtdgenome1.fasta"

# Directory for "raw" or trimmed fastq files, note the lack of last slash
dir_raw="/home/shaferab/projects/rrg-shaferab/shaferab/DEER"

# Output directory for bams, note the lack of last slash
dir_bam="/home/shaferab/projects/rrg-shaferab/shaferab/DEER"

# Option to load from a list of IDs:
# for f in `cat IDs`

for f in $(ls *.fastq.gz | cut -f1-4 -d'_'| uniq)
do
echo ${f}
sbatch -o /home/shaferab/projects/rrg-shaferab/shaferab/DEER/align_slurm/${f}-%A.out 04_bwa-sortSE.sh ${f} ${g} ${dir_raw} ${dir_bam}
sleep 10
done