#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=unique
#SBATCH --cpus-per-task=30
#SBATCH --mem=32G
#SBATCH --time=0-12:00 # time (DD-HH:MM)
module load gatk/3.8
module load sambamba
echo ${1}
java -Xmx30G -jar $EBROOTGATK/GenomeAnalysisTK.jar \
-T RealignerTargetCreator \
--use_jdk_deflater --use_jdk_inflater \
-nt 30 \
-R /home/shaferab/DEER/wtdgenome1.fasta\
-I /home/shaferab/DEER/DATA_ALIGNED_files/${1}.unique_reads.bam \
-o /home/shaferab/DEER/DATA_ALIGNED_files/${1}.realign.intervals &&
java -Xmx30G -jar $EBROOTGATK/GenomeAnalysisTK.jar \
-T IndelRealigner \
--use_jdk_deflater --use_jdk_inflater \
-R /home/shaferab/DEER/wtdgenome1.fasta\
-I /home/shaferab/DEER/DATA_ALIGNED_files/${1}.unique_reads.bam \
-targetIntervals /home/shaferab/DEER/DATA_ALIGNED_files/${1}.realign.intervals \
-o /home/shaferab/DEER/DATA_ALIGNED_files/${1}.realigned.bam &&
/home/shaferab/projects/rrg-shaferab/shaferab/DEER/sambamba-0.7.0-linux-static flagstat \
--nthreads=30 \
> /home/shaferab/DEER/DATA_ALIGNED_files/${1}.realigned.flagstat