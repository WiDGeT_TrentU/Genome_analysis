#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=bcftools_concat
#SBATCH --cpus-per-task=1
#SBATCH --mem=64G
#SBATCH --time=00-03:00

FILE="DEER_raw"
OUT="DEER"

module load plink/1.9b_5.2-x86_64

#Conduct linkage pruning first 
plink --vcf $FILE.vcf --double-id --allow-extra-chr --indep-pairphase 50 10 0.1 --out $OUT

plink --vcf $FILE.vcf --double-id --allow-extra-chr --extract $OUT.prune.in --make-bed --pca --out $OUT