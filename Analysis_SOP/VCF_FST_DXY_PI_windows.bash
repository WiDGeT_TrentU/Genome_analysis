#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=sliding_window
#SBATCH --ntasks=10
#SBATCH --time=03-23:00

#MEM / TIME ASK NEEDS TO BE ASSESSED


module load python/2.7.14
module load scipy-stack
FILE="DEER_raw"

#convert to geno file first  USING PYTHON SCRIPT
python parseVCF.py -i $FILE.vcf.gz -o $FILE.geno.gz

#For parameters see https://git.geomar.de/open-source/hamlets_ILD_vision_pigmentation; https://www.nature.com/articles/s41559-019-0814-5
python genomics_general/popgenWindows.py -g $FILE.geno.gz -o $FILE.50k.Fst.Dxy.pi.csv.gz \
   -f phased -w 50000 -s 5000 -m 10 --writeFailedWindows -T 10\
   -p East Odo_NB1_S13_L008,Odo_QC1_S12_L008 \
   -p Central Odo_VA3_S14_L008,Odo_ON1R_S11_L008,Odo_ON2R_S9_L008,Odo_ON3_S8_L008,Odo_ON4_S9_L008,Odo_ON5_S10_L008,Odo_ON6_S12_L008,Odo_ON7_S10_L008 \
   -p West Odo_AB1_S11_L008,Odo_SK2_S13_L008,Odo_SK3_S8_L008


python genomics_general/popgenWindows.py -g $FILE.geno.gz -o $FILE.10k.Fst.Dxy.pi.csv.gz \
   -f phased -w 10000 -s 1000 -m 10 --writeFailedWindows -T 10\
   -p East Odo_NB1_S13_L008,Odo_QC1_S12_L008 \
   -p Central Odo_VA3_S14_L008,Odo_ON1R_S11_L008,Odo_ON2R_S9_L008,Odo_ON3_S8_L008,Odo_ON4_S9_L008,Odo_ON5_S10_L008,Odo_ON6_S12_L008,Odo_ON7_S10_L008 \
   -p West Odo_AB1_S11_L008,Odo_SK2_S13_L008,Odo_SK3_S8_L008
