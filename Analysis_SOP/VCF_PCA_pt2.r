library(tidyverse)
library(ggplot2)
pca <- read_table2("./deer2.eigenvec", col_names = FALSE)
eigenval <- scan("./deer2.eigenval")
pca <- pca[,-1]
# set names
names(pca)[1] <- "ind"
names(pca)[2:ncol(pca)] <- paste0("PC", 1:(ncol(pca)-1))

#add species
spp <- rep(NA, length(pca$ind))
spp[grep("Odo", pca$ind)] <- "Odocoileus virginanus"
spp[grep("MD", pca$ind)] <- "Odocoileus hemionus"

#add location
loc <- rep(NA, length(pca$ind))
loc[grep("VA", pca$ind)] <- "Virginia"
loc[grep("SK", pca$ind)] <- "Saskatchewan"
loc[grep("QC", pca$ind)] <- "Quebec"
loc[grep("ON", pca$ind)] <- "Ontario"
#loc[grep("ON1R", pca$ind)] <- "Ontario (North)"
#loc[grep("ON7", pca$ind)] <- "Ontario (North)"
#loc[grep("ON5", pca$ind)] <- "Ontario (Central)"
#loc[grep("ON4", pca$ind)] <- "Ontario (South)"
#loc[grep("ON3", pca$ind)] <- "Ontario (South)"
#loc[grep("ON2R", pca$ind)] <- "Ontario (South)"
#loc[grep("ON3", pca$ind)] <- "Ontario (South)"
loc[grep("AB", pca$ind)] <- "Alberta"
loc[grep("NB", pca$ind)] <- "New Brunswick"
# combine - if you want to plot each in different colours
spp_loc <- paste0(spp, "_", loc)
pca <- as.tibble(data.frame(pca, spp, loc, spp_loc))
#1:no. samples
pve <- data.frame(PC = 1:13, pve = eigenval/sum(eigenval)*100)

a <- ggplot(pve, aes(PC, pve)) + geom_bar(stat = "identity")
a + ylab("Percentage variance explained") + theme_light()
cumsum(pve$pve)

b <- ggplot(pca, aes(PC1, PC2, col = spp, shape = loc)) + geom_point(size = 3)
b <- b + scale_colour_manual(values = c("red", "blue"))
b <- b + coord_equal() + theme_light()
b + xlab(paste0("PC1 (", signif(pve$pve[1], 3), "%)")) + ylab(paste0("PC2 (", signif(pve$pve[2], 3), "%)"))
